# IG Stress Auth 

IG Stress es un programa en consola con el cual puedes realizar solicitudes de autenticación al servicio web de Identity Guard, con la opción "loop" se podrá definir el número de solicitudes que el usuario requiera.

## Tipos de autenticaciones soportadas
 - Password
 - One Time Password (OTP)
## Funciones de disponibles 

  - Autenticación única 
  - Multiples solicitudes de autenticación


### Requerimientos


* Python 3.x.x 
* Identity Guard versión 11

### Sistemas compatibles
* Windows 7, 8 y 10 
* Linux (cualquier versión con python 3.x.x)

## Instalación

Clonar el repositorio



```sh
$ git clone https://gitlab.com/bryan-alfaro/identity-guard-stress-auth.git
```

Otra opción es la de descarga directa del proyecto 
[Descarga directa](https://gitlab.com/bryan-alfaro/identity-guard-stress-auth/-/archive/master/identity-guard-stress-auth-master.zip)

#### Instalar las dependencias

Windows

```sh
$ cd dentity-guard-stress-auth
$ python -m pip install -r requirements.txt
```
Linux

```sh
$ cd dentity-guard-stress-auth
$ python3 pip install -r requirements.txt
```

## Guía de uso

 ### Configuración 

Definir las urls de administración y autenticación de IdentityGuard en el archivo config.py
```python
    # Editar aqui
    urlAdmin = "https://igfenix.local:8444/IdentityGuardAdminService/services/AdminServiceV11"
    urlAuthSSL = "https://igfenix.local:8443/IdentityGuardAuthService/services/AuthenticationServiceV11"
    urlAuhtNoSSL = "http://igfenix.local:8080/IdentityGuardAuthService/services/AuthenticationServiceV11"
```
 ### Iniciar el programa

Windows PowerShell
```sh
$ python .\main.py
```
Linux
```sh
$ python3 main.py
```

Insertar el número de la función a utilizar en el  menú (1, 2 , 3 o 4)

```sh
#######################################################################
                    Identity Guard Strees 0.1 by Dante
#######################################################################


1 - getGenericChallenge
2 - Single authenticateGenericChallenge
3 - Loop authenticateGenericChallenge
4 - About

```
Definición de las opciones del menu


* **getGenericChallenge**: Esta opción realiza la solicitud de "response challenge", para más información consultar la documentación de Identity Guard.
* **Single authenticateGenericChallenge**: Esta opción realiza una solicitud de autenticación unica, es decir, en una sola ocasión.
* **Loop authenticateGenericChallenge**: Esta opción realiza múltiples solicitudes de autenticación automáticas definidas en el parámetro "loop".

### Utilizando "Single authenticateGenericChallenge" (2)

El intérprete solicitará 3 parámetros los cuales corresponden a:
*  **userId**: Nombre del usuario de Identity Guard
*  **authType**: Tipo de autenticación a utilizar la cual debe ser en mayúsculas  (PASSWORD, OTP )
*  **Response**: Este parámetro es la contraseña del usuario para el caso de un authType = PASSWORD o el One Time Password en caso de authType=OTP

```sh
Insert: userId authype response
_________________________________

Ana PASSWORD This15atest

```
Resultado
```sh
------------------
User Name: Ana
User Name: TokenGroup
Challenge History: PASSWORD
Last Failed Auth Type: PASSWORD
Last Failed Auth Date: 2019-06-07T12:22:47.000Z
*Login successful*
------------------
```
### Utilizando "Loop authenticateGenericChallenge" (3)

El intérprete solicitará  5 parámetros los cuales corresponden a:
*  **userId**: Nombre del usuario de Identity Guard.
*  **authType**: Tipo de autenticación a utilizar la cual debe ser en mayúsculas  (PASSWORD, OTP ).
*  **Response**: Este parámetro es la contraseña del usuario para el caso de un authType = PASSWORD o el One Time Password en caso de authType=OTP.
*  **loop**: Número de solicitudes automáticas. 
*  **sleep**: Tiempo de espera en segundos entre cada solicitud.

```sh
Insert: userId authype response loop sleep
_________________________________

Ana PASSWORD This15atest 2 3

```
Resultado
```sh
------------------
Attempt 1
User Name: Ana
User Name: TokenGroup
Challenge History: PASSWORD
Last Failed Auth Type: PASSWORD
Last Failed Auth Date: 2019-06-07T12:22:47.000Z
*Login successful*
------------------
------------------
Attempt 2
User Name: Ana
User Name: TokenGroup
Challenge History: PASSWORD
Last Failed Auth Type: PASSWORD
Last Failed Auth Date: 2019-06-07T12:22:47.000Z
*Login successful*
------------------
```

License
----

MIT

Author

Dante Alfaro

Sígueme en twitter

@DanteAlfarojs