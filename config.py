class config:

    '''
        En esta clase se definene la Configuracion de URLs

        Los siguientes son Ejemplos:
        urlAdmin = "https://localhost:8444/IdentityGuardAdminService/services/AdminServiceV11"
        urlAuthSSL = "https://localhost:8443/IdentityGuardAuthService/services/AuthenticationServiceV11"
        urlAuhtNoSSL = "http://localhost.local:8080/IdentityGuardAuthService/services/AuthenticationServiceV11"

    '''

    # Editar aqui
    urlAdmin = "https://igfenix.local:8444/IdentityGuardAdminService/services/AdminServiceV11"
    urlAuthSSL = "https://igfenix.local:8443/IdentityGuardAuthService/services/AuthenticationServiceV11"
    urlAuhtNoSSL = "http://igfenix.local:8080/IdentityGuardAuthService/services/AuthenticationServiceV11"
    # Editar informacio del Token
    TokenUserID = "Ana"
    TokenSerial = "17328-68375"
    TokenSet = "dante"
    TokenState = "CURRENT"  
    TokenVendorID = "Entrust Soft Token"

    # No editar 
    adminID = None
    password  = None    
    # set
    def setAdminID(self, adminid):
        self.adminID = adminid
    
    def setPassword(self, passw):
        self.password = passw

    # Getters de URLs
    def getUrlAdmin(self):
        return self.urlAdmin

    def getUrlAuthSSL(self):
        return self.urlAuthSSL

    def getUrlAuthNoSSL(self):
        return self.urlAuhtNoSSL
    
    def getAdminID(self):
        return self.adminID
    
    def getPasswordAD(self):
        return self.password


    

