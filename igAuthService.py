class AuthService:   

    '''
        Esta clase contiene las solicitudes 
        XML para el servicio SOAP de IG
        Las solicitudes agregadas son:

        - getGenericChallenge
        - authenticateGenericChallenge
        - Login Admin IG

    '''      

    def getGenericChallengeXML(self, *args, **kwargs):
        userId = kwargs["userId"]
        authType = kwargs["authtype"]
        raw =\
            '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:entrust.com:ig:authenticationV11:wsdl">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <urn:getGenericChallengeCallParms>
                            <userId>{0}</userId>
                            <parms>                                
                                <AuthenticationType>{1}</AuthenticationType>                            
                            </parms>
                        </urn:getGenericChallengeCallParms>
                    </soapenv:Body>
                 </soapenv:Envelope>
            '''
        xmlRequest = raw.format(userId, authType)
        return xmlRequest

    def authenticateGenericChallengeXML(self, *args, **kwargs):        
        userId = kwargs["userId"]
        authType = kwargs["authtype"]
        responseChallenge = kwargs["response"]
        raw =\
            '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:entrust.com:ig:authenticationV11:wsdl">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <urn:authenticateGenericChallengeCallParms>
                            <userId>{0}</userId>
                            <response>                                
                                <response>
                                    <item>{1}</item>
                                </response>
                            </response>
                            <parms>                            
                                <AuthenticationType>{2}</AuthenticationType>                                        
                            </parms>
                        </urn:authenticateGenericChallengeCallParms>
                    </soapenv:Body>
                 </soapenv:Envelope>
            '''        
        xmlRequest = raw.format(
            userId,            
            responseChallenge,
            authType,
        )
        return xmlRequest
    
    def loginAdminXML(self, admin, password):
        raw =\
            '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:entrust.com:ig:adminV11:wsdl">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <urn:loginCallParms>
                            <parms>
                                <adminId>{0}</adminId>
                                <password>{1}</password>                         
                            </parms>
                        </urn:loginCallParms>
                    </soapenv:Body>
                </soapenv:Envelope>
            '''
        xmlRequets = raw.format(admin, password)
        return xmlRequets
    
  


