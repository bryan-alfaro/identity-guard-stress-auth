import requests
import xmltodict
import json
from igAuthService import AuthService
from config import config

#Cargando config
myconfig = config()

class initRequest:
    
    ''' Esta clase contiene las solicitudes 
        HTTP para realizar 'Generic Challenge' y 
        'Authentication Challenge'
    '''

    def __init__(self):     
        self.urlAdmin = myconfig.getUrlAdmin()
        self.urlAuthSSL = myconfig.getUrlAuthSSL()
        self.urlAuthNoSSL = myconfig.getUrlAuthNoSSL()
    
    def getGenericChallengeRequests(self, *args, **kwargs):
        ''' Solicitud HTTP para el 
            SOAP Action: getGenericChallenge
        '''
        data = kwargs["data"]
        header = {'Content-Type': 'text/xml;charset=UTF-8', 'SOAPAction': '""'}    	
        req = requests.post(
            self.urlAuthSSL, 
            data=data,
            verify=False
        )        
        return req

    def authenticateGenericChallengeRequests(self, *args, **kwargs):

        '''
            Solicitud HTTP para el 
            SOAP Action: authenticateGenericChallenge
            PASSWORD and OTP AuthType
        '''

        data = kwargs["data"]
        headers = {
            "Content-Type": "text/xml;charset=UTF-8",        
            "SOAPAction": ""
        }    	
        req = requests.post(
            self.urlAuthSSL, 
            data=data,
            headers=headers,
            verify=False
        )        
        return req
    
    def LoginAdminRequets(self, data):

        '''
            Solicitud HTTP para
            Login en IG
        '''

        headers = {
            "Content-Type": "text/xml;charset=UTF-8",        
            "SOAPAction": ""
        }    	
        req = requests.post(
            self.urlAdmin, 
            data=data,
            headers=headers,
            verify=False
        )
        return req

    def authGenReqTOKENCR(self, *args, **kwargs):

        '''
            Solicitud HTTP para el 
            SOAP Action: authenticateGenericChallenge
            con AUTHTYPR TOKENCR
        '''        

        headers = {
            "Content-Type": "text/xml;charset=UTF-8",        
            "SOAPAction": ""
        }
        # Obteniendo XML del Login
        authLogin = AuthService()    
        xmlLogin = authLogin.loginAdminXML(
            admin=kwargs["admin"],        
            password=kwargs["password"]       
        )
        # Obteniando token de sesion
        reqLogin = self.LoginAdminRequets(data=xmlLogin)            
        JSESSIONID = reqLogin.headers['Set-Cookie']
        headers["Cookie"] = JSESSIONID 
        # XML obtener response Token                           
        XMLGetResponse =\
            '''
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:entrust.com:ig:adminV11:wsdl">
                <soapenv:Header/>
                <soapenv:Body>
                    <urn:userTokenResponseGetCallParms>
                        <userid>{0}</userid>
                        <filter>
                            <VendorId>{1}</VendorId>
                            <SerialNumber>{2}</SerialNumber>
                            <State>
                                <item>{3}</item>
                            </State>
                            <TokenSets>
                                <item>{4}</item>
                            </TokenSets>                        
                        </filter>                        
                    </urn:userTokenResponseGetCallParms>
                </soapenv:Body>
            </soapenv:Envelope>
            '''.format(
                    myconfig.TokenUserID,
                    myconfig.TokenVendorID,
                    myconfig.TokenSerial,
                    myconfig.TokenState,
                    myconfig.TokenSet
             )
        # Solcitud response token 
        reqGetResponseToken = requests.post(
            self.urlAdmin,
            data=XMLGetResponse,
            headers=headers,       
            verify=False
        )
        xmlParse = xmltodict.parse(reqGetResponseToken.text)  
        jsonParse = json.loads(json.dumps(xmlParse)) 
        body = jsonParse["soapenv:Envelope"]["soapenv:Body"]
        tokenResponse = body["userTokenResponseReturn"]["response"]["#text"]
        # XML AuthGeneric Token
        xmlgetAuthGenericToken =\
            '''
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:entrust.com:ig:authenticationV11:wsdl">
                <soapenv:Header/>
                <soapenv:Body>
                    <urn:authenticateGenericChallengeCallParms>
                        <userId>{0}</userId>
                        <response>                        
                            <response>                        
                                <item>{1}</item>
                            </response>                        
                        </response>
                        <parms>
                            <AuthenticationType>{2}</AuthenticationType>        
                            <serialNumber>{3}</serialNumber>
                            <tokenVendorId>{4}</tokenVendorId>
                            <tokenSets>                        
                                <item>{5}</item>
                            </tokenSets>                    
                        </parms>
                    </urn:authenticateGenericChallengeCallParms>
                </soapenv:Body>
            </soapenv:Envelope>
            '''.format(
                    myconfig.TokenUserID,
                    tokenResponse,
                    'TOKENCR',
                    myconfig.TokenSerial,
                    myconfig.TokenVendorID,
                    myconfig.TokenSet
            )      
        # Requets Auth Token 
        reqgetAuthtoken = requests.post(
            self.urlAuthSSL,
            headers=headers,
            data=xmlgetAuthGenericToken,
            verify=False
        )
        return reqGetResponseToken