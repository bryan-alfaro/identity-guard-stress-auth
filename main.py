import json
import time
import xmltodict
import asyncio
import time
import functools
import concurrent.futures
from igRequests import initRequest
from igAuthService import AuthService
from config import config
from datetime import datetime

'''
    Prueba de Stress para autenticacion
    de Identity Guard 

    Tipos de autenticacion soportados:
    - PASSWORD (No accesible)
    - TOKENCR

    Ultima actualizacion: 13/6/2019
    Autor: Dante Alfaro
'''

# cargando configuracion 
myconfig = config()

def pingAdminAuth():

    '''
        Funcion que comprueba
        las credenciales e inicio 
        de sesion
    '''

    auth = AuthService()    
    xml = auth.loginAdminXML(
        admin= myconfig.getAdminID(),        
        password= myconfig.getPasswordAD()        
    )
    req = initRequest()
    req = req.LoginAdminRequets(data=xml)
    return req

def AuthenticateGeneric(userId, authtype, rsp):

    '''
       Nota 12/06/2019: Esta funcion 
       no se utiliza 
       Esta funcion permite realizar 
       una solicitud de autenticacion 
       generica 
    '''

    # Preparando el XML con los parametros
    auth = AuthService()    
    xml = auth.authenticateGenericChallengeXML(
        userId=userId,         
        response=rsp,
        authtype=authtype, 
    )
    # Preparando la solicitud HTTP
    req = initRequest()
    req = req.authenticateGenericChallengeRequests(data=xml)  
    xmlParse = xmltodict.parse(req.text)  
    jsonParse = json.loads(json.dumps(xmlParse)) 
    body = jsonParse["soapenv:Envelope"]["soapenv:Body"]
    # Se verifica si el login fue exitoso o tuvo un fallo
    if "authenticateGenericChallengeReturn" in body.keys():
        body = body["authenticateGenericChallengeReturn"]
        print("\n")        
        print("------------------")
        print("User Name: " + body["userName"]["#text"])
        print("User Name: " + body["group"]["#text"])        
        print("*Login successful*")                
        print("------------------") 
        print("\n")
    elif "soapenv:Fault":
        body = body["soapenv:Fault"]
        print("\n")
        print("------------------") 
        print("Message: " + body["detail"]["ns1:AuthenticationServiceFault"]["ns1:message"])
        print("*login failed*")
        print("------------------")
        print("\n") 
    else:
        print("\n")
        print("Other error :(")     
        print("\n") 
            
async def LoopAuthenticateGeneric(userId, authtype, response, Nrequets):

    '''
        Nota 13/06/2019: Esta funcion 
        no se utiliza 
        Esta funcion realiza un
        loop de solicitudes 
        de authentication a IG de 
        manera asincrona.
    '''
    startTime = datetime.now()   
    with concurrent.futures.ThreadPoolExecutor() as executor:
        auth = AuthService()  
        # Solicitamos el payload XML s
        xml = auth.authenticateGenericChallengeXML(
            userId=userId, 
            authtype=authtype, 
            response=response
        )
        req = initRequest()
        # Iniciando el loop de las solicitudes async
        loop = asyncio.get_event_loop() 
        futures = [
            loop.run_in_executor(
                executor, 
                functools.partial(req.authenticateGenericChallengeRequests,
                    data=xml
                )                
            )                        
            for i in range(Nrequets)
        ]        
        for response in await asyncio.gather(*futures):
            if response.status_code == 200:
                print('Successful login')
            else:
                print('Fail login')     
    # Mostrar tiempos
    endTime = datetime.now()
    TotalTime = (endTime - startTime)    
    strStartTime = startTime.strftime("%H:%M:%S")  
    strEndTime = endTime.strftime("%H:%M:%S")     
    print("\n")
    print('Start Time: {0}'.format(strStartTime))
    print('End Time: {0}'.format(strEndTime))
    print('Total Time: {0}'.format(TotalTime))
    print("\n")

async def LoopAuthenticateGenericTOKENCR(Nrequets, admin, password):

    '''
        Esta funcion realiza un
        loop de solicitudes 
        de authentication a IG de 
        manera asincrona con el authtype
        TOKENCR.
    '''
    
    startTime = datetime.now()   
    with concurrent.futures.ThreadPoolExecutor() as executor:    
        # Iniciando el loop de las solicitudes async
        req = initRequest()       
        loop = asyncio.get_event_loop() 
        futures = [
            loop.run_in_executor(
                executor, 
                functools.partial(req.authGenReqTOKENCR,
                    admin=admin,
                    password=password
                ),
            )                        
            for i in range(Nrequets)
        ]        
        for response in await asyncio.gather(*futures):
            if response.status_code == 200:
                print('Successful login')
            else:
                print('Fail login')    
    # Mostrar tiempos
    endTime = datetime.now()
    TotalTime = (endTime - startTime)    
    strStartTime = startTime.strftime("%H:%M:%S")  
    strEndTime = endTime.strftime("%H:%M:%S")    
    print("\n")
    print('Start Time: {0}'.format(strStartTime))
    print('End Time: {0}'.format(strEndTime))
    print('Total Time: {0}'.format(TotalTime))
    print("\n")

if __name__ == "__main__":    
    
    # Credenciales Admin
    print("Insert admin and password\n")
    authAdmin = input()
    authAdmin = authAdmin.split(" ")
    # Guardando credenciales en memoria
    myconfig.setAdminID(authAdmin[0])  
    myconfig.setPassword(authAdmin[1])
    # Verificacion de credenciales admin
    print("login test")
    pingAdmin = pingAdminAuth()
    if  pingAdmin.status_code == 200:
        print('Login successful')
    else:
        print('Error http requets')
        print('HTTP Error: {0}'.format(
            pingAdmin.status_code
        ))
        exit()
    # Obteniendo el num de req
    print("Insert Num Requetst\n")
    requestNumber = input()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(LoopAuthenticateGenericTOKENCR(
            int(requestNumber),
            authAdmin[0],
            authAdmin[1]
        )
    ) 
